// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import { ApolloClient, createNetworkInterface } from 'apollo-client';
import VueApollo from 'vue-apollo';
import infiniteScroll from 'vue-infinite-scroll';
import App from './App';

const apolloClient = new ApolloClient({
  networkInterface: createNetworkInterface({
    uri: 'https://zjiezszxp5.execute-api.us-east-1.amazonaws.com/dev/graphql',
    transportBatching: true,
  }),
});

Vue.use(VueApollo, {
  apolloClient,
});

Vue.use(infiniteScroll);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
});
